# XNAT Central Deployment Configuration #

This is the XNAT Central deployment configuration. You can copy configuration files and
download plugin jars and the XNAT application by running:

```
$ ./gradlew
```

This copies all configuration files, plugin jars, and the XNAT war file to the folder
**build/deploy** in the same folder as your deployment configuration. You can specify 
another destination for plugins and configuration files by specifying the **xnatHome**
property when you run the deployment:


```
$ ./gradlew -PxnatHome=/data/xnat/home
```

This will copy all configuration files in the directory **src/main/config** to the folder
**${xnatHome}/config**, plugin jars to **${xnatHome}/plugins**, and the XNAT war file to
**${xnatHome}/webapps/ROOT.war** (note the renaming of the war file).

You can specify the exact location for configuration files and plugin jars separately
with the **configFolder** and **pluginFolder** properties:

```
$ ./gradlew -PconfigFolder=/data/xnat/home/config -PpluginFolder=/data/xnat/home/plugins
```

You can specify the folder and name for the XNAT war file with the properties
**warFolder** and **warName**:


```
$ ./gradlew -PwarFolder=/var/lib/tomcat7/webapps -PwarName=xnat.war
```

Finally you can specify the folder and name for the XNAT war file with a single property
**warFile**:

```
$ ./gradlew -PwarFile=/var/lib/tomcat7/webapps/xnat.war
```

Note that **warName** defaults to **ROOT.war**, so you can send the XNAT war file to
the Tomcat application folder as the root application with just the **warFolder**
property:

```
$ ./gradlew -PwarFolder=/var/lib/tomcat7/webapps
```

